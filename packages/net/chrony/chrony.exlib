# Copyright 2011 NAKAMURA Yoshitaka
# Distributed under the terms of the GNU General Public License v2

# TODO test/unit: "file not found with <angled> include; use "quotes" instead" with clang

require 66-service
require systemd-service [ systemd_files=[ "examples/chronyd.service" ] ]
require openrc-service [ openrc_confd_files=[ "${FILES}/openrc/confd/chrony" ] ]

export_exlib_phases src_install

SUMMARY="Versatile implementation of the Network Time Protocol"
HOMEPAGE="https://chrony.tuxfamily.org/"
DESCRIPTION="
chrony can synchronise the system clock with NTP servers, reference clocks (e.g. GPS receiver),
and manual input using wristwatch and keyboard. It can also operate as an NTPv4 (RFC 5905)
server and peer to provide a time service to other computers in the network."

if ever is_scm ; then
    SCM_REPOSITORY="git://git.tuxfamily.org/gitroot/${PN}/${PN}.git"
    require scm-git
else
    DOWNLOADS="https://download.tuxfamily.org/${PN}/${PNV}.tar.gz"
fi

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS="
    seccomp [[ description = [ Support for system call filtering ] ]]
"

DEPENDENCIES="
    build+run:
        dev-libs/libedit [[ description = [ Line editing support in chronyc ] ]]
        dev-libs/nss [[ description = [ Support NTP authentification with a symmetric key ] ]]
        sys-libs/libcap [[ description = [ Support dropping root privilegies ] ]]
        sys-libs/ncurses
        user/ntp
        seccomp? ( sys-libs/libseccomp )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}/2d9eb5b6fa5225a7300c8eed95712969249900fb.patch"
    "${FILES}/9a9c0d7b99d7478e1431bd3fa8c225bdc46b3df4.patch"
)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --localstatedir=/var # XXX: to avoid installing /var/lib/lib/chrony
    --sysconfdir=/etc/chrony
    --with-user=ntp
    --without-nettle # TODO NSS broken: nsslowhash.h: No such file
    --without-readline # force libedit
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    "seccomp scfilter"
)

chrony_src_install() {
    default

    dodoc -r examples

    insinto /etc/chrony
    doins "${FILES}/chrony.conf"

    keepdir /var/lib/chrony
    edo chown ntp "${IMAGE}"/var/lib/chrony

    install_66_files
    install_openrc_files
    install_systemd_files

    insinto /usr/$(exhost --target)/lib/systemd/ntp-units.d
    hereins 50-chrony.list <<EOF
chronyd.service
EOF
}

